const userJson = require('./users.json');

exports.login = (email, password) => {
    for (let user of userJson.users) {
        if (user.email == email && user.password == password) {
            return user;
        }
    }
};