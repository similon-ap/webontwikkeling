const express = require('express');
const users = require('./users');
const app = express();
app.set('port', 3000);
app.set('view engine', 'ejs');

app.use(express.static('public'));


app.get('/login', (req, res) => {


    const email = req.query.email;
    const password = req.query.password;

    if (!email || !password) {
        res.render('login', {error: ''});
        return;
    }

    let user = users.login(email, password);

    if (user) {
        res.render('main', user);
    } else {
        res.render('login', {error: 'Invalid login and password!'});
    }
});

app.listen(app.get('port'), () => {
    console.log(`Server started listening on ${app.get('port')}`);
});